# kb_garland-windows
## Visual Basic Script to create Christmas mood on your desktop. Just run this vbs file and look at the keypad, the LEDs will blink sequentially.
![They blink, I swear!](https://bitbucket.org/plaxin/kb_garland-windows/downloads/test-keyboard-demo.png)
They blink, I swear!
[Video](https://vk.com/video140830142_456239293)
***
1. Don't forget before you run the file to disable already enabled input modes from the keyboard!
2. The cycle lasts for almost a second, during this time, the LEDs light up and go off 1 time.
3. The frequency of cycles by default, spelled out in 20 thousand times.
4. ** IT IS NOT RECOMMENDED TO RUN SECONDARY PROCESSES! **
5. It is not recommended to change the delay between operations in a loop, will be visually ugly. :)

Made with love and fun by [Oleg Plaxin](https://bitbucket.org/plaxin)
